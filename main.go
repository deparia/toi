package main

import (
	"fmt"
	"strings"
)

// TEMP
func printAST(parent *Node, level int) {
	if parent == nil {
		return
	}

	if parent.token.tokenType == TOKEN_BEGIN_FILE {
		fmt.Printf("--- FILE %s ---\n", parent.token.contents)
	}

	for _, child := range parent.children {
		fmt.Printf(strings.Repeat("> ", level))
		if child != nil {
			//fmt.Printf("Parent: %s $ child:", parent.token.contents)
			fmt.Println(child.token.contents, "|", child.token.newline, "-", child.token.indentation)
			printAST(child, level + 1)
		} else {
			if level != 0 {
				fmt.Println("nil")
			}
		}

		if level == 0 {
			fmt.Println()
		}
	}
}

func printOperations(operations []Operation) {
	fmt.Println()
	fmt.Println("AST converted to operations:")
	for _, op := range operations {
		fmt.Println(op)
	}
	fmt.Println()
}

func main() {
	parser := NewParser()

	parser.ParseFile("foo.toi")

	named         := SetTokenNames(parser.tokens)
	ast           := TokensToAST(named)
	checked       := TypeCheckAST(ast)
	rawOperations := ASTToOperations(checked)
	operations    := TypeCheckOperations(rawOperations)

	InterpretOperations(operations)
}
