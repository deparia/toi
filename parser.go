package main

import (
	"fmt"
	"os"
)

type TokenType int
type BuiltinType int

const (
	TOKEN_ILLEGAL TokenType = iota

	// Signals the beginning of a file
	TOKEN_BEGIN_FILE

	// If token is builtin, lookup builtin
	TOKEN_BUILTIN

	// Symbols
	TOKEN_PLUS
	TOKEN_LPAREN
	TOKEN_RPAREN
	TOKEN_COLON
	TOKEN_EQUALS
	TOKEN_LESSTHAN

	// String
	TOKEN_STRING

	// Numbers
	TOKEN_INT

	NUM_TOKENTYPES int = iota - 1
)

const (
	BUILTIN_UNKNOWN BuiltinType = iota

	BUILTIN_PRINT
	BUILTIN_PRINTLN

	BUILTIN_PROC
	BUILTIN_END

	BUILTIN_FUNC
	BUILTIN_BEGIN

	BUILTIN_TRUE
	BUILTIN_FALSE

	BUILTIN_IF
	BUILTIN_THEN
	BUILTIN_ELSE

	BUILTIN_GLOBAL

	BUILTIN_WHILE
	BUILTIN_DO

	NUM_BUILTINS int = iota - 1
)

var (
	SYMBOL_TO_TOKEN = map[byte]TokenType{
		'+': TOKEN_PLUS,
		'(': TOKEN_LPAREN,
		')': TOKEN_RPAREN,
		':': TOKEN_COLON,
		'=': TOKEN_EQUALS,
		'<': TOKEN_LESSTHAN,
	}

	TOKEN_TO_SYMBOL = map[TokenType]byte{
		TOKEN_PLUS:     '+',
		TOKEN_LPAREN:   '(',
		TOKEN_RPAREN:   ')',
		TOKEN_COLON:    ':',
		TOKEN_EQUALS:   '=',
		TOKEN_LESSTHAN: '<',
	}

	WORD_TO_BUILTIN = map[string]BuiltinType{
		"print":   BUILTIN_PRINT,
		"println": BUILTIN_PRINTLN,
		"proc":    BUILTIN_PROC,
		"end":     BUILTIN_END,
		"true":    BUILTIN_TRUE,
		"false":   BUILTIN_FALSE,
		"if":      BUILTIN_IF,
		"then":    BUILTIN_THEN,
		"else":    BUILTIN_ELSE,
		"global":  BUILTIN_GLOBAL,
		"while":   BUILTIN_WHILE,
		"do":      BUILTIN_DO,
		"func":    BUILTIN_FUNC,
		"begin":   BUILTIN_BEGIN,
	}

	BUILTIN_TO_WORD = map[BuiltinType]string{
		BUILTIN_PRINT:      "print",
		BUILTIN_PRINTLN:    "println",
		BUILTIN_PROC:       "proc",
		BUILTIN_END:        "end",
		BUILTIN_TRUE:       "true",
		BUILTIN_FALSE:      "false",
		BUILTIN_IF:         "if",
		BUILTIN_THEN:       "then",
		BUILTIN_ELSE:       "else",
		BUILTIN_GLOBAL:     "global",
		BUILTIN_WHILE:      "while",
		BUILTIN_DO:         "do",
		BUILTIN_FUNC:       "func",
		BUILTIN_BEGIN:      "begin",
	}
)

type Location struct {
	filename string
	line int
	column int
}

func (location *Location) Formatted() string {
	return fmt.Sprintf("%s:%d:%d", location.filename, location.line, location.column)
}

type Token struct {
	tokenType TokenType
	builtinType BuiltinType

	location Location
	contents string

	indentation int
	newline bool
}

func (token *Token) Formatted() string {
	switch token.tokenType {
	case TOKEN_BUILTIN:
		if _, ok := BUILTIN_TO_WORD[token.builtinType]; ok {
			return token.contents
		}

	case TOKEN_STRING:
		return fmt.Sprintf("\"%s\"", token.contents)

	default:
		return token.contents
	}

	return token.contents
}

type Parser struct {
	cursor   int
	location Location

	char   byte
	token  Token

	text   []byte
	tokens []Token
}

func NewParser() Parser {
	return Parser{
		location: Location{
			// this used to be 1, 1
			// what should this be?
			line: 1,
			column: 0,
		},
	}
}

func (parser *Parser) PeekChar() (byte, bool) {
	if parser.cursor >= len(parser.text) {
		return 0, false
	}

	return parser.text[parser.cursor], true
}

func (parser *Parser) PeekWord() (string, bool) {
	if parser.cursor >= len(parser.text) {
		return "", false
	}

	var (
		cursor int = parser.cursor - 1
		wording bool = true
		word string
		inWord bool
	)

	if cursor < 0 { return "", false }

	for wording {
		if cursor >= len(parser.text) {
			return word, true
		}

		if _, ok := SYMBOL_TO_TOKEN[parser.text[cursor]]; ok {
			wording = false
		} else if IsSpace(parser.text[cursor]) {
			wording = false
		} else if inWord && IsWordMiddle(parser.text[cursor]) {
			word += string(parser.text[cursor])
		} else if IsWordStart(parser.text[cursor]) {
			word += string(parser.text[cursor])
			inWord = true
		} else {
			return "", false
		}

		cursor += 1
	}

	return word, true
}

func (parser *Parser) NextChar() bool {
	if parser.cursor >= len(parser.text) {
		return false
	}

	if parser.char == '\n' {
		parser.location.line += 1
		parser.location.column = 0
	}

	parser.char = parser.text[parser.cursor]

	parser.cursor += 1
	parser.location.column += 1

	return true
}

func (parser *Parser) JumpChars(n int) {
	var origin int = parser.cursor
	for parser.cursor != origin + n - 1 && parser.NextChar() { }
}

func (parser *Parser) GetIndentation() (int, bool) {
	var (
		cursor int = parser.cursor
		initialCursor int = cursor
		linestart int
		indentation int
		newline bool = true
	)

	foundLinefeed := false
	for !foundLinefeed {
		cursor -= 1

		if cursor < 0 || parser.text[cursor] == '\n' {
			foundLinefeed = true
		}
	}

	cursor += 1
	linestart = cursor

	for cursor < len(parser.text) && parser.text[cursor] == '\t'{
		cursor += 1
		indentation += 1
	}

	cursor = linestart

	// Check if newline
	for cursor < len(parser.text) && cursor < initialCursor {
		if parser.text[cursor] != '\t' {
			newline = false
		}

		cursor += 1
	}

	return indentation, newline
}

func (parser *Parser) NextToken() bool {
	parser.token = Token{}

	// TODO: this is just brute force
	parser.token.indentation, parser.token.newline = parser.GetIndentation()

	if !parser.NextChar() {
		return false
	}

	if tokenType, ok := SYMBOL_TO_TOKEN[parser.char]; ok {
		parser.token.tokenType = tokenType
		parser.token.location  = parser.location
		parser.token.contents  = string(parser.char)
		return true
	}

	if parser.char == '"' {
		parser.token.tokenType = TOKEN_STRING
		parser.token.location  = parser.location

		stringing := true

		for stringing && parser.NextChar() {
			if parser.char == '"' {
				stringing = false
			} else {
				parser.token.contents += string(parser.char)
			}
		}

		// If stringing was not set to false the string was never closed.
		if stringing {
			parser.token.tokenType = TOKEN_ILLEGAL
			parser.token.contents = "\"" + parser.token.contents
			Error("Syntax error: unclosed string.", parser.token.location)
			Exit()
		}

		return true
	}

	if IsSpace(parser.char) {
		return parser.NextToken()
	}

	if IsDigit(parser.char) {
		parser.token.contents += string(parser.char)
		parser.token.location = parser.location

		stop := false
		for !stop {
			peeked, ok := parser.PeekChar()

			if ok && IsDigit(peeked) {
				parser.NextChar()
				parser.token.contents += string(parser.char)
			} else {
				stop = true
			}
		}

		parser.token.tokenType = TOKEN_INT
		return true
	}

	peeked, ok := parser.PeekWord()

	if ok {
		parser.token.location = parser.location
		parser.JumpChars(len(peeked))

		if builtinType, ok := WORD_TO_BUILTIN[peeked]; ok {
			parser.token.tokenType   = TOKEN_BUILTIN
			parser.token.builtinType = builtinType
		} else {
			parser.token.tokenType = TOKEN_ILLEGAL
		}

		parser.token.contents = peeked
		return true
	}

	parser.token.tokenType = TOKEN_ILLEGAL
	parser.token.location  = parser.location
	parser.token.contents  = string(parser.char)
	return true
}

func (parser *Parser) ParseFile(filename string) {
	text, err := os.ReadFile(filename)
	if err != nil {
		fmt.Printf("ERROR: Could not %s.\n", err)
	}

	if len(WORD_TO_BUILTIN) != len(BUILTIN_TO_WORD) {
		panic("Parser assertion failed: builtins must be convertable both ways.")
	}

	if len(SYMBOL_TO_TOKEN) != len(TOKEN_TO_SYMBOL) {
		panic("Parser assertion failed: tokens must be convertable both ways.")
	}

	if NUM_BUILTINS != len(BUILTIN_TO_WORD) {
		panic("Parser assertion failed: builtins must have a text representation")
	}

	parser.location.filename = filename
	parser.text = text

	// Create file token
	parser.tokens = append(parser.tokens, Token{
		tokenType: TOKEN_BEGIN_FILE,
		builtinType: BUILTIN_UNKNOWN,

		location: parser.location,
		contents: filename,

		indentation: -1,
		newline: true,
	})

	for parser.NextToken() {
		parser.tokens = append(parser.tokens, parser.token)
	}
}

