package main

import "fmt"

// This is not a joke
type TypeType int

const (
	TYPE_UNKNOWN TypeType = iota

	TYPE_UNTYPED_STRING
	TYPE_UNTYPED_INT
	TYPE_UNTYPED_BOOL

	TYPE_U64

	NUM_TYPES int = iota - 1
)

var (
	TYPE_TO_STRING = map[TypeType]string{
		TYPE_UNTYPED_STRING: "untyped string",
		TYPE_UNTYPED_INT:    "untyped int",
		TYPE_UNTYPED_BOOL:   "untyped bool",
		TYPE_U64:            "u64",
	}

	STRING_TO_TYPE = map[string]TypeType{
		"u64": TYPE_U64,
	}

	global_variables = make(map[string]TypeType)
)

func UNTYPEDTOKEN_TO_TYPE(t TokenType, b BuiltinType) TypeType {
	if t == TOKEN_INT {
		return TYPE_UNTYPED_INT
	} else if t == TOKEN_STRING {
		return TYPE_UNTYPED_STRING
	} else if t == TOKEN_BUILTIN {
		if b == BUILTIN_TRUE || b == BUILTIN_FALSE {
			return TYPE_UNTYPED_BOOL
		}
	}

	return TYPE_UNKNOWN
}

func typeIsNumber(t TypeType) bool {
	return t == TYPE_U64 || t == TYPE_UNTYPED_INT
}

func canAssignUntypedType(type_, variable TypeType) bool {
	if type_ == variable {
		return true
	}

	if type_ == TYPE_UNTYPED_INT {
		if variable == TYPE_U64 {
			return true
		}
	}

	return false
}

type Type struct {
	typeType TypeType
	location Location
}

func CreateTypes(parent *Node) {
	for _, child := range parent.children {
		if child != nil {
			CreateTypes(child)
		}
	}

	if parent.token.tokenType == TOKEN_ILLEGAL {
		type_, ok := STRING_TO_TYPE[parent.token.contents]
		if ok {
			parent.type_ = type_
		}
	} else {
		parent.type_ = UNTYPEDTOKEN_TO_TYPE(parent.token.tokenType, parent.token.builtinType)
	}
}

// Returns an array of types to typecheck against (this function is recursive).
func TypeCheckNode(root *Node, inArgs bool) []TypeType {
	if NUM_TOKENTYPES != 10 {
		panic("typechecker assertion failed: NUM_TOKENTYPES does not have expected value.")
	}

	if NUM_BUILTINS != 14 {
		panic("typechecker assertion failed: NUM_BUILTINS does not have expected value.")
	}

	switch root.token.tokenType {
	case TOKEN_ILLEGAL:
		if _, ok := global_variables[root.token.contents]; ok {
			root.nodeType = NODE_VARIABLE
			root.data1    = root.token.contents

			if !inArgs {
				Error("Unhandled variable.", root.token.location)
				Exit()
			}

			return []TypeType{global_variables[root.token.contents]}
		}

	case TOKEN_BEGIN_FILE:
		for _, child := range root.children {
			TypeCheckNode(child, false)
		}

	case TOKEN_BUILTIN:
		switch root.token.builtinType {
		case BUILTIN_PROC:
			if len(root.children) > 0 {
				Error("Procedure definitions do not accept input arguments.", root.token.location)

				for i, n := range root.children {
					Info(fmt.Sprintf("Argument %d originates here.", i+1), n.token.location)
				}

				Exit()
			}

		case BUILTIN_FUNC:
			// TODO: implement function arguments
			if len(root.children) > 0 {
				Error("Function definitions do not accept input arguments.", root.token.location)

				for i, n := range root.children {
					Info(fmt.Sprintf("Argument %d originates here.", i+1), n.token.location)
				}

				Exit()
			}

		case BUILTIN_BEGIN:
			if len(root.children) > 0 {
				Error("'begin' does not accept any arguments.", root.token.location)
				Exit()
			}

		case BUILTIN_PRINT: fallthrough
		case BUILTIN_PRINTLN:
			// Print and println accept any and all arguments
			for _, child := range root.children {
				TypeCheckNode(child, true)
			}

		case BUILTIN_END:
			if len(root.children) > 0 {
				Error("'end' does not accept any arguments.", root.token.location)
				Exit()
			}

		case BUILTIN_IF:
			if len(root.children) == 0 {
				Error("Expected condition for if statement, found nothing.", root.token.location)
				Exit()
			}

			// TODO: how do we do typechecking in if statements?
			for _, child := range root.children {
				TypeCheckNode(child, true)
			}

		case BUILTIN_WHILE:
			if len(root.children) == 0 {
				Error("Expected condition for while, found nothing.", root.token.location)
				Exit()
			}

			// TODO: how do we do typechecking in while conditions?
			for _, child := range root.children {
				TypeCheckNode(child, true)
			}

		case BUILTIN_THEN:
			if len(root.children) > 0 {
				Error("'then' does not accept any arguments.", root.token.location)
				Exit()
			}

		case BUILTIN_DO:
			if len(root.children) > 0 {
				Error("'do' does not accept any arguments.", root.token.location)
				Exit()
			}

		case BUILTIN_ELSE:
			if len(root.children) > 0 {
				Error("'else' does not accept any arguments.", root.token.location)
				Exit()
			}

		case BUILTIN_TRUE: fallthrough
		case BUILTIN_FALSE:
			if !inArgs {
				Error("Unhandled boolean.", root.token.location)
				Exit()
			}
			return []TypeType{TYPE_UNTYPED_BOOL}

		case BUILTIN_GLOBAL:
			// TODO: typecheck global

		default:
			panic(fmt.Sprintf("internal error: unhandled builtin '%s'", BUILTIN_TO_WORD[root.token.builtinType]))
		}

	case TOKEN_STRING:
		if !inArgs {
			Error("Unhandled string.", root.token.location)
			Exit()
		}
		return []TypeType{TYPE_UNTYPED_STRING}

	case TOKEN_INT:
		if !inArgs {
			Error("Unhandled integer.", root.token.location)
			Exit()
		}
		return []TypeType{TYPE_UNTYPED_INT}

	case TOKEN_PLUS: fallthrough
	case TOKEN_LESSTHAN:
		var typeArray []TypeType
		for _, child := range root.children {
			typeArray = append(typeArray, TypeCheckNode(child, true)...)
		}

		return typeArray

		// TODO: Infix operators should get typechecked here instead of in TypeCheckOperations()

	case TOKEN_LPAREN: fallthrough
	case TOKEN_RPAREN:
		Error("Syntax error: unexpected parenthesis.", root.token.location)
		Exit()

	case TOKEN_COLON:
		if len(root.children) != 2 && len(root.children) != 3 {
			panic(fmt.Sprintf("internal error: incorrect number of children (expected 2 or 3, got %d)",  len(root.children)))
		}

		nameToken := root.children[0].token
		typeToken := root.children[1].token

		if nameToken.tokenType != TOKEN_ILLEGAL {
			Error(fmt.Sprintf("Cannot use '%s' as the name of a variable.", nameToken.contents), nameToken.location)
			Exit()
		}

		if typeToken.tokenType != TOKEN_ILLEGAL {
			Error(fmt.Sprintf("Cannot use '%s' as the type for a variable.", typeToken.contents), typeToken.location)
			Exit()
		}

		if root.children[0].type_ != TYPE_UNKNOWN {
			Error(fmt.Sprintf("Cannot use type '%s' as the name of a variable.", nameToken.contents), nameToken.location)
			Exit()
		}

		if root.children[1].type_ == TYPE_UNKNOWN {
			Error(fmt.Sprintf("Unknown type '%s'.", typeToken.contents), typeToken.location)
			Exit()
		}

		if len(root.children) == 3 &&
			root.children[2].token.tokenType != TOKEN_INT &&
			root.children[2].token.tokenType != TOKEN_STRING {
			Error(fmt.Sprintf("Cannot assign '%s' to a variable.", root.children[2].token.contents), root.children[2].token.location)
			Exit()
		}

		global_variables[nameToken.contents] = root.children[1].type_

		// TODO: currently you cannot assign anything that is not a single node.
		// eg. summarizing two numbers
		root.nodeType = NODE_NEW_VARIABLE
		root.data1    = nameToken.contents
		if len(root.children) == 3 {
			root.data2 = root.children[2].token.contents
		}
		root.data2_t = root.children[1].type_

		root.children = []*Node{}

	case TOKEN_EQUALS:
		if len(root.children) != 2 {
			panic(fmt.Sprintf("internal error: incorrect number of children (expected 2, got %d)",  len(root.children)))
		}

		nameToken  := root.children[0].token
		valueToken := root.children[1].token

		if nameToken.tokenType != TOKEN_ILLEGAL {
			Error(fmt.Sprintf("Cannot assign to '%s' (not a variable).", nameToken.contents), nameToken.location)
			Exit()
		}

		if root.children[0].type_ != TYPE_UNKNOWN {
			Error(fmt.Sprintf("Cannot assign to '%s' (not a variable).", nameToken.contents), nameToken.location)
			Exit()
		}

		if IsInfix(valueToken.tokenType) {
			// TODO: typecheck this
			for _, child := range root.children {
				TypeCheckNode(child, true)
			}

			// TODO: Infix operators should get typechecked here instead of in TypeCheckOperations()
		} else if valueToken.tokenType == TOKEN_ILLEGAL || valueToken.tokenType == TOKEN_BUILTIN {
			returned := TypeCheckNode(root.children[1], true)

			if len(returned) > 1 {
				panic("multiple return values are not implemented")
			}

			if len(returned) == 0 {
				Error(fmt.Sprintf("'%s' returns no value to assign to '%s' (variable of type '%s').",
					valueToken.contents,
					nameToken.contents,
					TYPE_TO_STRING[global_variables[nameToken.contents]]),
					root.token.location)
				Exit()
			}

			if returned[0] != global_variables[nameToken.contents] && !canAssignUntypedType(returned[0], global_variables[nameToken.contents]) {
				Error(fmt.Sprintf("Cannot assign '%s' (value of type '%s') to '%s' (variable of type '%s').",
					valueToken.contents,
					TYPE_TO_STRING[returned[0]],
					nameToken.contents,
					TYPE_TO_STRING[global_variables[nameToken.contents]]),
					root.token.location)
				Exit()
			}
		} else if !canAssignTokenType(global_variables[nameToken.contents], valueToken.tokenType) {
			if root.children[1].type_ != TYPE_UNKNOWN {
				Error(fmt.Sprintf("Cannot assign '%s' (value of type '%s') to '%s' (variable of type '%s').",
					valueToken.contents,
					TYPE_TO_STRING[root.children[1].type_],
					nameToken.contents,
					TYPE_TO_STRING[global_variables[nameToken.contents]]),
					root.token.location)
			} else {
				Error(fmt.Sprintf("Cannot assign '%s' to '%s' (variable of type '%s').",
					valueToken.contents,
					nameToken.contents,
					TYPE_TO_STRING[global_variables[nameToken.contents]]),
					root.token.location)
			}

			Exit()
		}

		root.data1   = nameToken.contents
		root.data2   = valueToken.contents
		root.data2_t = root.children[1].type_

		root.children[0] = root.children[1]
		root.children = root.children[:len(root.children)-1]

	default:
		panic(fmt.Sprintf("internal error: unhandled token %d.", root.token.tokenType))
	}

	return []TypeType{}
}

func canAssignTokenType(type_ TypeType, tokenType TokenType) bool {
	if NUM_TYPES != 4 {
		panic("typechecker assertion failed: NUM_TYPES does not have expected value")
	}

	switch type_ {
	case TYPE_U64:
		if tokenType == TOKEN_INT { return true }
	}

	return false
}

func StripTypes(parent *Node) bool {
	if parent.type_ != TYPE_UNKNOWN {
		if len(parent.children) > 0 {
			Error("Types do not take in input arguments.", parent.token.location)
			Exit()
		}

		return true
	}

	for i, child := range parent.children {
		if child != nil {
			if StripTypes(child) {
				parent.children[i] = nil
			}
		}
	}

	return false
}

func TypeCheckAST(ast *Node) *Node {
	CreateTypes(ast)
	TypeCheckNode(ast, false)
	//StripTypes(ast)
	return ast
}

func ListTypestack(typestack []Type) {
	for i, t := range typestack {
		Info(fmt.Sprintf("Argument %d of type %s originates here.", i+1, TYPE_TO_STRING[t.typeType]), t.location)
	}
}

func TypeCheckOperations(operations []Operation) []Operation {
	var (
		operation Operation
		typestack []Type
	)

	if NUM_OPERATIONS != 23 {
		panic("typechecker assertion failed: NUM_OPERATIONS does not have expected value.")
	}

	for i := 0; i < len(operations); i++ {
		operation = operations[i]

		switch operation.operationType {
		case OPERATION_ILLEGAL:
			Error(fmt.Sprintf("Unknown word '%s'.", operation.contents), operation.location)
			Exit()
			// Do nothing

		case OPERATION_GLOBAL:
			i += 1
			if operations[i].operationType != OPERATION_NEW_VARIABLE {
				Error("Cannot mark non-variable as global.", operations[i].location)
				Info("Global was used here.", operation.location)
				Exit()
			}

		case OPERATION_NEW_VARIABLE:
			// Variable declarations get checked at an earlier stage

		case OPERATION_ASSIGN_VARIABLE:
			if len(typestack) == 0 {
				Error("Expected value to assign to variable, found nothing.", operation.location)
				Exit()
			}

			typestack = typestack[:len(typestack)-1]

		case OPERATION_PUSH_VARIABLE:
			typestack = append(typestack, Type{
				typeType: global_variables[operation.contents],
				location: operation.location,
			})

		// expects only 1 bool, only bool for now, ints in the future
		case OPERATION_IF: fallthrough
		case OPERATION_WHILE:
			if len(typestack) > 1 {
				Error(fmt.Sprintf("Too many arguments to condition. (Expected 1, got %d)", len(typestack)), operation.location)
				ListTypestack(typestack)
				Exit()
			}

			if len(typestack) == 0 {
				Error("Expected condition, found nothing.", operation.location)
				Exit()
			}

			top      := typestack[len(typestack)-1]
			typestack = typestack[:len(typestack)-1]

			if top.typeType != TYPE_UNTYPED_BOOL {
				Error(fmt.Sprintf("Expected boolean, found %s.", TYPE_TO_STRING[top.typeType]), operation.location)
				Exit()
			}

		case OPERATION_ELSE:
			if len(typestack) > 0 {
				Error("'else' does not accept any arguments.", operation.location)
				ListTypestack(typestack)
				Exit()
			}

		case OPERATION_END_IF:
		case OPERATION_END_ELSE:
		case OPERATION_END_WHILE:
		case OPERATION_DO:
			// Do nothing
			// We should remove these operation types.

		case OPERATION_BEGIN_PROC:
		case OPERATION_BEGIN_FUNC:
			// Do nothing
			// Procedure and function typechecking happens elsewhere

		case OPERATION_RETURN:

		case OPERATION_CALL_PROC:
			if len(typestack) > 0 {
				Error("Procedures do not accept input arguments.", operation.location)

				for i, t := range typestack {
					Info(fmt.Sprintf("Argument %d originates here.", i+1), t.location)
				}

				Exit()
			}

		case OPERATION_CALL_FUNC:
			// TODOOOOO: Implement typechecking for function calls

		case OPERATION_PUSH_INT:
			typestack = append(typestack, Type{
				typeType: TYPE_UNTYPED_INT,
				location: operation.location,
			})

		case OPERATION_PRINT:
			// Print accepts any and all arguments
			if len(typestack) < 1 {
				Error(fmt.Sprintf("Operation 'print' expects at least 1 argument."), operation.location)
				Exit()
			}

			typestack = []Type{}

		case OPERATION_PRINTLN:
			// Println prints a single \n if no arguments get passed
			typestack = []Type{}

		case OPERATION_PUSH_STRING:
			typestack = append(typestack, Type{
				typeType: TYPE_UNTYPED_STRING,
				location: operation.location,
			})

		case OPERATION_PUSH_BOOL:
			typestack = append(typestack, Type{
				typeType: TYPE_UNTYPED_BOOL,
				location: operation.location,
			})

		case OPERATION_PLUS:
			typestack = typeCheckInfix(operation, typestack)

		case OPERATION_LESSTHAN:
			typestack = typeCheckInfixComparison(operation, typestack)

		default:
			panic(fmt.Sprintf("internal error: unhandled operation %d.", operation.operationType))
		}
	}

	if len(typestack) > 0 {
		Error("Unhandled value.", typestack[0].location)
		Exit()
	}

	return operations
}

// <num> <infix> <num>
// ->
// <num>
func typeCheckInfixComparison(operation Operation, typestack []Type) []Type {
	if len(typestack) < 2 {
		Error(fmt.Sprintf("Operation '%s' expects 2 arguments.", operation.contents), operation.location)
		Exit()
	}

	a        := typestack[len(typestack)-1]
	typestack = typestack[:len(typestack)-1]

	a_s, ok := TYPE_TO_STRING[a.typeType]
	if !ok {
		panic("internal error: unexpected type on typestack")
	}

	b        := typestack[len(typestack)-1]
	typestack = typestack[:len(typestack)-1]

	b_s, ok := TYPE_TO_STRING[b.typeType]
	if !ok {
		panic("internal error: unexpected type on typestack")
	}

	if !typeIsNumber(a.typeType) {
		Error(fmt.Sprintf("Cannot use type '%s' as argument to '%s' (not a number).", a_s, operation.contents), a.location)
		Exit()
	}

	if !typeIsNumber(b.typeType) {
		Error(fmt.Sprintf("Cannot use type '%s' as argument to '%s' (not a number).", b_s, operation.contents), b.location)
		Exit()
	}

	typestack = append(typestack, Type{
		typeType: TYPE_UNTYPED_BOOL,
		location: operation.location,
	})

	return typestack
}

// <num> <infix> <num>
// ->
// <num>
func typeCheckInfix(operation Operation, typestack []Type) []Type {
	if len(typestack) < 2 {
		Error(fmt.Sprintf("Operation '%s' expects 2 arguments.", operation.contents), operation.location)
		Exit()
	}

	a        := typestack[len(typestack)-1]
	typestack = typestack[:len(typestack)-1]

	a_s, ok := TYPE_TO_STRING[a.typeType]
	if !ok {
		panic("internal error: unexpected type on typestack")
	}

	b        := typestack[len(typestack)-1]
	typestack = typestack[:len(typestack)-1]

	b_s, ok := TYPE_TO_STRING[b.typeType]
	if !ok {
		panic("internal error: unexpected type on typestack")
	}

	if !typeIsNumber(a.typeType) {
		Error(fmt.Sprintf("Cannot use type '%s' as argument to '%s' (not a number).", a_s, operation.contents), a.location)
		Exit()
	}

	if !typeIsNumber(b.typeType) {
		Error(fmt.Sprintf("Cannot use type '%s' as argument to '%s' (not a number).", b_s, operation.contents), b.location)
		Exit()
	}

	if a.typeType == TYPE_UNTYPED_INT {
		typestack = append(typestack, Type{
			typeType: b.typeType,
			location: operation.location,
		})
		return typestack
	}

	if b.typeType == TYPE_UNTYPED_INT {
		typestack = append(typestack, Type{
			typeType: a.typeType,
			location: operation.location,
		})
		return typestack
	}

	if a.typeType == b.typeType {
		typestack = append(typestack, Type{
			typeType: a.typeType,
			location: operation.location,
		})
		return typestack
	}

	Error(fmt.Sprintf("Cannot use different types as arguments to '%s' (got '%s' and '%s').", operation.contents, a_s, b_s), operation.location)
	Info(fmt.Sprintf("Argument 1 of type '%s' originates here.", a_s), a.location)
	Info(fmt.Sprintf("Argument 2 of type '%s' originates here.", b_s), b.location)
	Exit()

	return typestack
}
