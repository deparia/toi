package main

import "fmt"

type NodeType int
const (
	NODE_UNKNOWN NodeType = iota

	NODE_NEW_VARIABLE
	NODE_VARIABLE

	NUM_NODETYPES int = iota - 1
)

type Node struct {
	token Token
	children []*Node

	type_ TypeType
	nodeType NodeType

	data1 string
	data1_t TypeType
	data2 string
	data2_t TypeType
}

func parseCall(tokens []Token) (Node, int) {
	var (
		currToken int
		root      Node
	)

	root.token = tokens[0]
	currToken += 2 // Skip root and opening parenthesis

	for currToken < len(tokens) && tokens[currToken].tokenType != TOKEN_RPAREN {
		expr, n := parseExpression(tokens[currToken:])
		root.children = append(root.children, &expr)
		currToken += n
	}

	currToken += 1 // Skip closing parenthesis

	return root, currToken
}

func parseVariable(tokens []Token) (Node, int) {
	var (
		currToken int
		root      Node
	)

	// Variable name
	root.children = append(root.children, &Node{
		token: tokens[0],
		children: nil,
	})
	currToken += 1

	// Colon
	root.token = tokens[1]
	currToken += 1

	if len(tokens) == 2 {
		Error("Reached end of file before end of declaration.", tokens[1].location)
		Info("Declaration started here.", tokens[0].location)
		Exit()
	}

	if tokens[2].tokenType == TOKEN_EQUALS {
		panic("unimplemented feature")
	} else {
		if tokens[2].tokenType != TOKEN_ILLEGAL {
			Error(fmt.Sprintf("Expected variable type, found '%s'.", tokens[2].contents), tokens[2].location)
			Exit()
		}

		// Variable type
		root.children = append(root.children, &Node{
			token: tokens[2],
			children: nil,
		})
		currToken += 1

		if len(tokens) >= 4 && tokens[3].tokenType == TOKEN_EQUALS {
			if len(tokens) == 4 {
				Error("Reached end of file before end of declaration", tokens[3].location)
				Info("Declaration started here.", tokens[0].location)
				Exit()
			}

			// Equals
			currToken += 1

			// Variable value
			root.children = append(root.children, &Node{
				token: tokens[4],
				children: nil,
			})
			currToken += 1
		}
	}

	return root, currToken
}

func parseIf(tokens []Token) (Node, int) {
	var (
		root Node
		currToken int
	)

	root.token = tokens[0]
	currToken += 1 // Skip opening 'if'

	if len(tokens) == 2 {
		Error("Reached end of file before end of condition.", tokens[1].location)
		Exit()
	}

	if tokens[1].tokenType == TOKEN_BUILTIN && tokens[1].builtinType == BUILTIN_THEN {
		Error("Empty if condition.", tokens[0].location)
		Exit()
	}

	// FIXME: A 'then' without a starting 'if' makes the program hang because
	// it hits break without incrementing. Maybe we should keep track of
	// whether or not it should break or Error()? This could be passed around
	// as just an integer, as a block does not need to know its parents state.

	// Condition
	for currToken < len(tokens) && tokens[currToken].builtinType != BUILTIN_THEN {
		expr, n := parseExpression(tokens[currToken:])
		root.children = append(root.children, &expr)
		currToken += n
	}

	currToken += 1 // Skip closing 'then'

	return root, currToken
}

func parseWhile(tokens []Token) (Node, int) {
	var (
		root Node
		currToken int
	)

	// While
	root.token = tokens[0]
	currToken += 1

	if len(tokens) == 2 {
		Error("Reached end of file before end of condition.", tokens[1].location)
		Exit()
	}

	if tokens[1].tokenType == TOKEN_BUILTIN && tokens[1].builtinType == BUILTIN_DO {
		Error("Empty while condition.", tokens[0].location)
		Exit()
	}

	// FIXME: A 'then' without a starting 'if' makes the program hang.
	//        This may also apply for 'while'-blocks.
	//        See FIXME in parseIf()

	// Condition
	for currToken < len(tokens) && tokens[currToken].builtinType != BUILTIN_DO {
		expr, n := parseExpression(tokens[currToken:])
		root.children = append(root.children, &expr)
		currToken += n
	}

	// Do
	root.children = append([]*Node{&Node{
		token: tokens[currToken],
		children: nil,
	}}, root.children...)
	currToken += 1

	return root, currToken
}

func parseVariableAssignment(tokens []Token) (Node, int) {
	var (
		currToken int
		root      Node
	)

	// Variable name
	root.children = append(root.children, &Node{
		token: tokens[0],
		children: nil,
	})
	currToken += 1

	// Equals
	root.token = tokens[1]
	currToken += 1

	if len(tokens) == 2 {
		Error("Reached end of file before end of declaration.", tokens[1].location)
		Info("Declaration started here.", tokens[0].location)
		Exit()
	}

	// Variable value
	expr, n := parseExpression(tokens[currToken:])
	root.children = append(root.children, &expr)
	currToken += n

	return root, currToken
}

func IsInfix(t TokenType) bool {
	if NUM_TOKENTYPES != 10 {
		panic("ast generator assertion failed: NUM_TOKENTYPES does not have expected value")
	}

	if t == TOKEN_PLUS || t == TOKEN_LESSTHAN {
		return true
	}

	return false
}

func parseInfix(tokens []Token) (Node, int) {
	var (
		currToken int
		root      Node
	)

	// Left
	root.children = append(root.children, &Node{
		token: tokens[0],
		children: nil,
	})
	currToken += 1

	// Infix operator
	root.token = tokens[1]
	currToken += 1

	if len(tokens) == 2 {
		Error("Infix operator '%s' expects 2 arguments, one on each side.", tokens[1].location)
		Exit()
	}

	// Right
	expr, n := parseExpression(tokens[currToken:])
	root.children = append(root.children, &expr)
	currToken += n

	return root, currToken

}

func parseFunction(tokens []Token) (Node, int) {
	if len(tokens) == 0 {
		panic("internal error: tokens is not supposed to be empty.")
	}

	var (
		root      Node
		currToken int
	)

	fmt.Println(tokens)

	// TODO: Pick up from here.
	// Implement function parsing. Start by implementing input arguments.
	// You might have to implement local variables in the interpreter first...
	//
	// Example:
	// func print_sum
	//     a: u64
	//     b: u64
	// begin
	//     println(a + b)
	// end

	panic("function parsing is not implemented")
	return root, currToken
}

func parseExpression(tokens []Token) (Node, int) {
	if len(tokens) == 0 {
		panic("internal error: tokens is not supposed to be empty.")
	}

	var (
		node      Node
		currToken int
	)

	if currToken + 1 < len(tokens) && IsInfix(tokens[currToken + 1].tokenType) {
		child, n := parseInfix(tokens[currToken:])
		node = child
		currToken += n
	} else if tokens[currToken].tokenType == TOKEN_LPAREN {
		currToken += 1
		child, n := parseExpression(tokens[currToken:])
		node = child
		currToken += n
	} else if tokens[currToken].builtinType == BUILTIN_FUNC {
		child, n := parseFunction(tokens[currToken:])
		node = child
		currToken += n
	} else if currToken + 1 < len(tokens) && tokens[currToken + 1].tokenType == TOKEN_LPAREN {
		child, n := parseCall(tokens[currToken:])
		node = child
		currToken += n
	} else if currToken + 1 < len(tokens) && tokens[currToken + 1].tokenType == TOKEN_COLON {
		child, n := parseVariable(tokens[currToken:])
		node = child
		currToken += n
	} else if currToken + 1 < len(tokens) && tokens[currToken + 1].tokenType == TOKEN_EQUALS {
		child, n := parseVariableAssignment(tokens[currToken:])
		node = child
		currToken += n
	} else if tokens[currToken].tokenType == TOKEN_BUILTIN && tokens[currToken].builtinType == BUILTIN_IF {
		child, n := parseIf(tokens[currToken:])
		node = child
		currToken += n
	} else if tokens[currToken].tokenType == TOKEN_BUILTIN && tokens[currToken].builtinType == BUILTIN_WHILE {
		child, n := parseWhile(tokens[currToken:])
		node = child
		currToken += n
	} else if tokens[currToken].tokenType == TOKEN_RPAREN {
		// TODO: when ()() it prioritizes next ( instead of closing first )
		currToken += 1 // For call parsing
	} else if tokens[currToken].tokenType == TOKEN_BUILTIN && tokens[currToken].builtinType == BUILTIN_THEN {
		currToken += 1 // For if condition parsing
	} else if tokens[currToken].tokenType == TOKEN_BUILTIN && tokens[currToken].builtinType == BUILTIN_DO {
		currToken += 1 // For while parsing
	} else {
		child := Node{
			token: tokens[currToken],
			children: nil,
		}

		node = child
		currToken += 1
	}

	return node, currToken
}

func TokensToAST(tokens []Token) *Node {
	if len(tokens) == 0 {
		return nil
	}

	var (
		ast Node
		currToken int
	)

	// File token
	ast.token = tokens[0]
	currToken += 1

	for currToken < len(tokens) {
		child, n := parseExpression(tokens[currToken:])
		ast.children = append(ast.children, &child)
		currToken += n
	}

	return &ast
}
