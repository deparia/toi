package main

// This file has functions and types related to tois intermediate representation
// Any list of operations should be executable operation by operation

import (
	"fmt"
)

type OperationType int

const (
	OPERATION_ILLEGAL OperationType = iota

	OPERATION_PUSH_STRING
	OPERATION_PUSH_INT
	OPERATION_PUSH_BOOL

	OPERATION_PLUS
	OPERATION_LESSTHAN

	OPERATION_PRINT
	OPERATION_PRINTLN

	OPERATION_BEGIN_PROC
	OPERATION_CALL_PROC

	OPERATION_BEGIN_FUNC
	OPERATION_CALL_FUNC

	OPERATION_RETURN

	OPERATION_IF
	OPERATION_END_IF // Currently only exists for jump offsets, should get removed!

	OPERATION_ELSE
	OPERATION_END_ELSE // Currently only exists for jump offsets, should get removed!

	OPERATION_WHILE
	OPERATION_DO // Currently only exists for jump offsets, should get removed!
	OPERATION_END_WHILE // Currently only exists for jump offsets, should get removed!

	OPERATION_GLOBAL // Tells the interpreter to mark this variable as global
	OPERATION_NEW_VARIABLE
	/* OPERATION_NEW_VARIABLE
		contents: name of variable
		data1:    value to assign to variable
		data1_t:  type of value to assign to variable
	*/
	OPERATION_PUSH_VARIABLE
	OPERATION_ASSIGN_VARIABLE
	/* OPERATION_ASSIGN_VARIABLE
		contents: name of variable
		data1_t:  type of value to assign to variable
	*/
	// TODO: we should not keep track of the type of the value we assign

	NUM_OPERATIONS int = iota - 1
)

type Operation struct {
	operationType OperationType
	contents string

	jumpOffset int
	data1 string
	data1_t TypeType

	location Location
}

func ReverseOperations(ops []Operation) []Operation {
	if len(ops) <= 1 {
		return ops
	}

	for i, j := 0, len(ops)-1; i < j; i, j = i+1, j-1 {
		ops[i], ops[j] = ops[j], ops[i]
	}

	return ops
}

func procExists(name string, procs []string) bool {
	for _, n := range procs {
		if n == name {
			return true
		}
	}

	return false
}

func ASTToOperations(ast *Node) []Operation {
	var (
		// Gets incremented in when in procedure or function
		procAndFuncStack int

		stack []Node
		node    Node

		operations []Operation
	)

	if NUM_TOKENTYPES != 10 {
		panic("IR generator assertion failed: NUM_TOKENTYPES does not have expected value.")
	}

	if NUM_BUILTINS != 14 {
		panic("IR generator assertion failed: NUM_BUILTINS does not have expected value.")
	}

	if NUM_OPERATIONS != 23 {
		panic("IR generator assertion failed: NUM_OPERATIONS does not have expected value.")
	}

	// Push root
	stack = append(stack, *ast)

	for len(stack) > 0 {
		// Pop last node
		node  = stack[len(stack)-1]
		stack = stack[:len(stack)-1]

		for _, child := range node.children {
			if child != nil {
				stack = append(stack, *child)
			}
		}

		var operation Operation

		operation.location = node.token.location
		operation.contents = node.token.contents

		if procAndFuncStack == 0 && node.token.tokenType != TOKEN_BEGIN_FILE {
			if node.token.builtinType != BUILTIN_PROC &&
				node.token.builtinType != BUILTIN_END &&
				node.token.tokenType != TOKEN_COLON &&
				node.token.builtinType != BUILTIN_GLOBAL {
				if node.nodeType != NODE_NEW_VARIABLE {
					Error(fmt.Sprintf("Words are not allowed at the top level of the program. (Found '%s')", node.token.contents), node.token.location)
					Exit()
				}
			}
		}

		switch node.token.tokenType {
		case TOKEN_ILLEGAL:
			if node.nodeType == NODE_VARIABLE {
				operation.operationType = OPERATION_PUSH_VARIABLE
			}

			operations = append(operations, operation)

		case TOKEN_BEGIN_FILE:
			// Do nothing

		case TOKEN_BUILTIN:
			switch node.token.builtinType {
			// NOTE: Because of the way we traverse the AST the token to end a block gets processed first
			//       That's why we decrement in BEGIN_PROC and increment in END.
			//       The name of a block should have gotten set previously.
			case BUILTIN_PROC:
				operation.operationType = OPERATION_BEGIN_PROC

				procAndFuncStack -= 1

				operations = append(operations, operation)

			case BUILTIN_FUNC:
				operation.operationType = OPERATION_BEGIN_FUNC

				procAndFuncStack -= 1

				operations = append(operations, operation)

			case BUILTIN_END:
				// Check what block it closes
				if operation.contents == BUILTIN_TO_WORD[BUILTIN_IF] {
					operation.operationType = OPERATION_END_IF
					operations = append(operations, operation)
				} else if operation.contents == BUILTIN_TO_WORD[BUILTIN_ELSE] {
					operation.operationType = OPERATION_END_ELSE
					operations = append(operations, operation)
				} else if operation.contents == BUILTIN_TO_WORD[BUILTIN_WHILE] {
					operation.operationType = OPERATION_END_WHILE
					operations = append(operations, operation)
				} else {
					// Assumes procedure or function
					operation.operationType = OPERATION_RETURN

					procAndFuncStack += 1

					operations = append(operations, operation)
				}

			case BUILTIN_PRINT:
				operation.operationType = OPERATION_PRINT
				operations = append(operations, operation)

			case BUILTIN_PRINTLN:
				operation.operationType = OPERATION_PRINTLN
				operations = append(operations, operation)

			case BUILTIN_TRUE: fallthrough
			case BUILTIN_FALSE:
				operation.operationType = OPERATION_PUSH_BOOL
				operations = append(operations, operation)

			case BUILTIN_IF:
				operation.operationType = OPERATION_IF
				operations = append(operations, operation)

			case BUILTIN_WHILE:
				operation.operationType = OPERATION_WHILE
				operations = append(operations, operation)

			case BUILTIN_THEN:
			case BUILTIN_BEGIN:
				// Do nothing

			case BUILTIN_DO:
				operation.operationType = OPERATION_DO
				operations = append(operations, operation)

			case BUILTIN_ELSE:
				operation.operationType = OPERATION_ELSE
				operations = append(operations, operation)

			case BUILTIN_GLOBAL:
				operation.operationType = OPERATION_GLOBAL
				operations = append(operations, operation)

			default:
				panic(fmt.Sprintf("internal error: unhandled builtin '%s'", BUILTIN_TO_WORD[node.token.builtinType]))
			}

		case TOKEN_STRING:
			operation.operationType = OPERATION_PUSH_STRING
			operations = append(operations, operation)

		case TOKEN_INT:
			operation.operationType = OPERATION_PUSH_INT
			operations = append(operations, operation)

		case TOKEN_PLUS:
			operation.operationType = OPERATION_PLUS
			operations = append(operations, operation)

		case TOKEN_LESSTHAN:
			operation.operationType = OPERATION_LESSTHAN
			operations = append(operations, operation)

		case TOKEN_COLON:
			operation.operationType = OPERATION_NEW_VARIABLE

			operation.contents = node.data1
			// TODO: remove this
			operation.data1    = node.data2
			operation.data1_t  = node.data2_t

			// Default values for variables
			if operation.data1 == "" {
				if operation.data1_t == TYPE_U64 {
					operation.data1 = "0"
				}
			}
			if NUM_TYPES != 4 {
				panic("IR generator assertion failed: NUM_TYPES does not have expected value")
			}

			operations = append(operations, operation)

		case TOKEN_EQUALS:
			operation.operationType = OPERATION_ASSIGN_VARIABLE

			operation.contents = node.data1
			// TODO: remove this
			operation.data1    = node.data2
			operation.data1_t  = node.data2_t

			operations = append(operations, operation)

		default:
			panic(fmt.Sprintf("internal error: unhandled token %d", node.token.tokenType))
		}
	}

	return SetJumpOffsets(operations)
}
