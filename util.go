package main

import (
	"fmt"
	"os"
)

func IsWordMiddle(char byte) bool {
	return IsWordStart(char) || IsDigit(char)
}

func IsWordStart(char byte) bool {
	return IsLetter(char) || char == '_'
}

func IsLetter(char byte) bool {
	lowercase := int('a') <= int(char) && int(char) <= int('z')
	uppercase := int('A') <= int(char) && int(char) <= int('Z')
	return lowercase || uppercase
}

func IsSpace(char byte) bool {
	return char == ' ' || char == '\r' || char == '\n' || char == '\t'
}

func IsDigit(char byte) bool {
	return int('0') <= int(char) && int(char) <= int('9')
}

func Info(text string, location Location) {
	fmt.Printf("%s: [INFO] %s\n", location.Formatted(), text)
}

func Error(text string, location Location) {
	fmt.Fprintf(os.Stderr, "%s: [ERROR] %s\n", location.Formatted(), text)
}

func Exit() {
	os.Exit(1)
}

