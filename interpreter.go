package main

import (
	"fmt"
	"strconv"
)

type Value struct {
	contents string
	location Location
}

// Represents the index into the operation stack to jump to.
type CallStackItem int

type Variable struct {
	variableType TypeType
	value string
}

func InterpretOperations(operations []Operation) {
	var (
		callStack []CallStackItem

		stack []Value
		value   Value

		global_variables = make(map[string]Variable) // variable name -> variable value

		local_variables_stack []map[string]Variable // variable name -> variable value
		local_variables *map[string]Variable
	)

	if NUM_OPERATIONS != 23 {
		panic("Interpreter assertion failed: NUM_OPERATIONS does not have expected value.")
	}

	local_variables_stack = append(local_variables_stack, make(map[string]Variable))
	local_variables = &local_variables_stack[0]

	for i := 0; i < len(operations); i++ {
		operation := operations[i]

		value = Value{}

		switch operation.operationType {
		case OPERATION_ILLEGAL:
			panic("internal error: illegal operation passed to interpreter")

		case OPERATION_NEW_VARIABLE:
			(*local_variables)[operations[i].contents] = Variable{
				variableType: operations[i].data1_t,
				value: operations[i].data1,
			}

		case OPERATION_ASSIGN_VARIABLE:
			if v, ok := global_variables[operation.contents]; ok {
				top  := stack[len(stack)-1]
				stack = stack[:len(stack)-1]

				v.value = top.contents
				global_variables[operation.contents] = v
			} else if v, ok := (*local_variables)[operation.contents]; ok {
				top  := stack[len(stack)-1]
				stack = stack[:len(stack)-1]

				v.value = top.contents
				(*local_variables)[operation.contents] = v
			}

		case OPERATION_PUSH_VARIABLE:
			if v, ok := global_variables[operation.contents]; ok {
				value.contents = v.value
			} else if v, ok := (*local_variables)[operation.contents]; ok {
				value.contents = v.value
			}
			stack = append(stack, value)

		case OPERATION_GLOBAL:
			i += 1
			global_variables[operations[i].contents] = Variable{
				variableType: operations[i].data1_t,
				value: operations[i].data1,
			}

		case OPERATION_IF:
			cond := stack[len(stack)-1]
			stack = stack[:len(stack)-1]

			//True,  _ := BUILTIN_TO_WORD[BUILTIN_TRUE]
			False, _ := BUILTIN_TO_WORD[BUILTIN_FALSE]

			// The typechecker makes sure if receives a boolean
			// Only booleans are supported at the moment
			if cond.contents == False {
				i += operation.jumpOffset
			}

		case OPERATION_WHILE:
			cond := stack[len(stack)-1]
			stack = stack[:len(stack)-1]

			//True,  _ := BUILTIN_TO_WORD[BUILTIN_TRUE]
			False, _ := BUILTIN_TO_WORD[BUILTIN_FALSE]

			if cond.contents == False {
				i += operation.jumpOffset
			}

		case OPERATION_ELSE:
			i += operation.jumpOffset

		case OPERATION_END_IF:
		case OPERATION_END_ELSE:
		case OPERATION_DO:
			// Do nothing
			// We should remove these operation types.

		case OPERATION_END_WHILE:
			i += operation.jumpOffset

		case OPERATION_PRINT:
			for _, v := range stack {
				fmt.Print(v.contents)
			}

			stack = []Value{}

		case OPERATION_PRINTLN:
			for _, v := range stack {
				fmt.Print(v.contents)
			}

			fmt.Println()

			stack = []Value{}

		case OPERATION_BEGIN_PROC:
			i += operation.jumpOffset

		case OPERATION_BEGIN_FUNC:
			i += operation.jumpOffset

		case OPERATION_CALL_PROC:
			callStack = append(callStack, CallStackItem(i))
			i += operation.jumpOffset

			local_variables_stack = append(local_variables_stack, make(map[string]Variable))
			local_variables = &local_variables_stack[len(local_variables_stack)-1]

		case OPERATION_CALL_FUNC:
			callStack = append(callStack, CallStackItem(i))
			i += operation.jumpOffset

			local_variables_stack = append(local_variables_stack, make(map[string]Variable))
			local_variables = &local_variables_stack[len(local_variables_stack)-1]

		case OPERATION_RETURN:
			loc      := callStack[len(callStack)-1]
			callStack = callStack[:len(callStack)-1]

			local_variables_stack = local_variables_stack[:len(local_variables_stack)-1]
			local_variables = &local_variables_stack[len(local_variables_stack)-1]

			i = int(loc)

		case OPERATION_PUSH_STRING:
			value.contents = operation.contents
			stack = append(stack, value)

		case OPERATION_PUSH_INT:
			value.contents = operation.contents
			stack = append(stack, value)

		case OPERATION_PUSH_BOOL:
			// TODO: When if conditions get implemented we need to rethink this.
			// Currently a runtime value is always a string. Not good. (maybe we change that to an array of bytes?)
			value.contents = operation.contents
			stack = append(stack, value)

		case OPERATION_PLUS:
			top  := stack[len(stack)-1]
			stack = stack[:len(stack)-1]
			a, _ := strconv.ParseInt(top.contents, 10, 64)

			top   = stack[len(stack)-1]
			stack = stack[:len(stack)-1]
			b, _ := strconv.ParseInt(top.contents, 10, 64)

			value.contents = strconv.Itoa(int(a + b))
			stack = append(stack, value)

		case OPERATION_LESSTHAN:
			top  := stack[len(stack)-1]
			stack = stack[:len(stack)-1]
			a, _ := strconv.ParseInt(top.contents, 10, 64)

			top   = stack[len(stack)-1]
			stack = stack[:len(stack)-1]
			b, _ := strconv.ParseInt(top.contents, 10, 64)

			if b < a {
				value.contents = "true"
			} else {
				value.contents = "false"
			}

			stack = append(stack, value)

		default:
			panic(fmt.Sprintf("internal error: unhandled operation %d", operation.operationType))
		}
	}
}
