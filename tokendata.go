package main

import "fmt"

func SetTokenNames(tokens []Token) []Token {
	var (
		output []Token
		token    Token

		blockStack []Token
	)

	for i := 0; i < len(tokens); i++ {
		token = tokens[i]

		// The file tokens name gets manually set when the parser starts a new file.
		// We therefore don't have to handle it here.

		if token.tokenType == TOKEN_BUILTIN {
			if token.builtinType == BUILTIN_PROC {
				i += 1

				if tokens[i].tokenType == TOKEN_ILLEGAL {
					token.contents = tokens[i].contents
					blockStack = append(blockStack, token)
				} else {
					Error(fmt.Sprintf("Cannot assign '%s' as the name of a procedure.", tokens[i].contents), tokens[i].location)
					Info("Procedure starts here.", token.location)
					Exit()
				}
			} else if token.builtinType == BUILTIN_FUNC {
				i += 1

				if tokens[i].tokenType == TOKEN_ILLEGAL {
					token.contents = tokens[i].contents
					blockStack = append(blockStack, token)
				} else {
					Error(fmt.Sprintf("Cannot assign '%s' as the name of a function.", tokens[i].contents), tokens[i].location)
					Info("Function starts here.", token.location)
					Exit()
				}
			} else if token.builtinType == BUILTIN_IF {
				blockStack = append(blockStack, token)
			} else if token.builtinType == BUILTIN_WHILE {
				blockStack = append(blockStack, token)
			} else if token.builtinType == BUILTIN_ELSE {
				if len(blockStack) == 0 {
					Error("Found 'else', but no accompanying 'if'.", token.location)
					Exit()
				}

				if blockStack[len(blockStack)-1].builtinType != BUILTIN_IF {
					Error("Found 'else', but no accompanying 'if'.", token.location)
					Exit()
				}

				blockStack = append(blockStack, token)
			} else if token.builtinType == BUILTIN_END {
				if len(blockStack) == 0 {
					Error("Closing unopened block.", token.location)
					Exit()
				}

				if blockStack[len(blockStack)-1].builtinType == BUILTIN_ELSE {
					blockStack = blockStack[:len(blockStack)-1]
				}

				token.contents = blockStack[len(blockStack)-1].contents
				blockStack     = blockStack[:len(blockStack)-1]
			}
		}

		output = append(output, token)
	}

	if len(blockStack) > 0 {
		Error("Reached end of file before end of block.", tokens[len(tokens)-1].location)

		for _, b := range blockStack {
			Info("This block was never closed.", b.location)
		}

		Exit()
	}

	return output
}

func SetFunctionOffsets(operations []Operation) []Operation {
	var (
		output []Operation

		prevBlockI int
		procs = make(map[string]int)
		funcs = make(map[string]int)
	)

	for i, operation := range operations {
		if operation.operationType == OPERATION_BEGIN_PROC {
			procs[operation.contents] = i

			operation.jumpOffset = i - prevBlockI
		} else if operation.operationType == OPERATION_BEGIN_FUNC {
			funcs[operation.contents] = i

			operation.jumpOffset = i - prevBlockI
		} else if operation.operationType == OPERATION_RETURN {
			prevBlockI = i
		}

		output = append(output, operation)
	}

	return SetFunctionCalls(output, procs, funcs)
}

func SetIfOffsets(operations []Operation) []Operation {
	var (
		output []Operation
		blockStack []int
	)

	// NOTE: Since we know there is something after 'else' we can safely point
	//       the if statement at the operation after 'else'.

	for i, operation := range operations {
		if operation.operationType == OPERATION_IF {
			operation.jumpOffset = i - blockStack[len(blockStack)-1]
			blockStack           = blockStack[:len(blockStack)-1]
		} else if operation.operationType == OPERATION_ELSE {
			operation.jumpOffset = i - blockStack[len(blockStack)-1]
			blockStack           = blockStack[:len(blockStack)-1]

			blockStack = append(blockStack, i)
		} else if operation.operationType == OPERATION_END_IF {
			blockStack = append(blockStack, i)
		} else if operation.operationType == OPERATION_END_ELSE {
			blockStack = append(blockStack, i)
		}

		output = append(output, operation)
	}

	return output
}

func SetWhileOffsets(operations []Operation) []Operation {
	var blockStack []int

	for i := 0; i < len(operations); i++ {
		if operations[i].operationType == OPERATION_WHILE {
			operations[i].jumpOffset = i - blockStack[len(blockStack)-1]
			blockStack = blockStack[:len(blockStack)-1]
		} else if operations[i].operationType == OPERATION_END_WHILE {
			blockStack = append(blockStack, i)
		}
	}

	for i := len(operations)-1; i > 0; i-- {
		if operations[i].operationType == OPERATION_DO {
			blockStack = append(blockStack, i)
		} else if operations[i].operationType == OPERATION_END_WHILE {
			operations[i].jumpOffset = i - blockStack[len(blockStack)-1]
			blockStack = blockStack[:len(blockStack)-1]
		}
	}

	return operations
}

func SetJumpOffsets(operations []Operation) []Operation {
	return SetFunctionOffsets(SetWhileOffsets(SetIfOffsets(operations)))
}

func SetFunctionCalls(operations []Operation, procs map[string]int, funcs map[string]int) []Operation {
	var (
		output []Operation
	)

	// Manually set main entry point
	operations = append([]Operation{Operation{
		operationType: OPERATION_CALL_PROC,
		contents: "main",
	}}, operations...)

	for i, operation := range operations {
		if operation.operationType == OPERATION_ILLEGAL || operation.operationType == OPERATION_CALL_PROC || operation.operationType == OPERATION_CALL_FUNC {
			if loc, ok := procs[operation.contents]; ok {
				operation.operationType = OPERATION_CALL_PROC
				operation.jumpOffset = i - loc - 1
			} else if loc, ok := funcs[operation.contents]; ok {
				operation.operationType = OPERATION_CALL_FUNC
				operation.jumpOffset = i - loc - 1
			}
		}

		output = append(output, operation)
	}

	return ReverseOperations(output)
}
